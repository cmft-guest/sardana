Source: sardana
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Picca Frédéric-Emmanuel <picca@debian.org>, Carlos Pascual <cpascual@cells.es>
Section: science
Priority: optional
Build-Depends: debhelper (>= 10),
               dh-python,
               python-all,
               python-setuptools,
Build-Depends-Indep: dvipng,
                     gir1.2-hkl-5.0,
                     graphviz,
                     python-itango,
                     python-lxml,
                     python-tango,
                     python-sphinx,
                     python-sphinx-rtd-theme,
                     python-taurus,
                     rdfind,
                     symlinks,
                     texlive-latex-base,
                     texlive-latex-extra
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/science-team/sardana
Vcs-Git: https://salsa.debian.org/science-team/sardana.git
Homepage: http://www.sardana-controls.org

Package: python-sardana
Architecture: all
Section: python
Depends: ${misc:Depends},
         ${python:Depends},
         gir1.2-hkl-5.0,
         python-itango,
         python-lxml,
         python-h5py,
         python-tango,
         python-taurus
Suggests: python-sardana-doc
Breaks: python-taurus (<< 3.3.0+dfsg-2~)
Provides: ${python:Provides}
Replaces: python-taurus (<< 3.3.0+dfsg-2~)
Description: instrument control and data acquisition system - library
 Sardana is a Supervision, Control And Data Acquisition (SCADA) system for
 scientific installations. It is written in Python and based on the TANGO
 library. The hardware control and data acquisition routines can be
 accessed via an IPython console and a generic graphical user interface
 (both of which are easily extensible by the user).

Package: python-sardana-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: instrument control and data acquisition system - documentation
 Sardana is a Supervision, Control And Data Acquisition (SCADA) system for
 scientific installations. It is written in Python and based on the TANGO
 library. The hardware control and data acquisition routines can be
 accessed via an IPython console and a generic graphical user interface
 (both of which are easily extensible by the user).
 .
 This package contains the documentation for the python-sardana library.
